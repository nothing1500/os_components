#include <stdio.h>

int main() {
    // Open a file in write mode ('w')
    FILE *file = fopen("test.txt", "w");
    if (file == NULL) {
        printf("Error: Unable to open the file for writing.\n");
        return 1;
    }

    // Write some text into the file
    fprintf(file, "Hello World.\n");
    fprintf(file, "My name is Pankaj Bhatt.\n");

    // Close the file
    fclose(file);

    // Open the file in read mode ('r')
    file = fopen("test.txt", "r");
    if (file == NULL) {
        printf("Error: Unable to open the file for reading.\n");
        return 1;
    }

    // Read and print the file's content
    char ch;
    while ((ch = fgetc(file)) != EOF) {
        printf("%c", ch);
    }

    // Close the file
    fclose(file);

    return 0;
}