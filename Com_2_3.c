#include <stdio.h>
#include <stdlib.h>
#include <time.h>
#include <string.h>

#define MAX_RANDOM_NUMBER 5000
#define INPUT_BUFFER_SIZE 10

// Function prototype
void gameLoop(int randomNumber);

int main() {
    srand(time(NULL));
    int randomNumber = rand() % MAX_RANDOM_NUMBER + 1;

    printf("Welcome to the number guessing game!\n");
    printf("Guess a number between 1 and %d. Enter 0 to end the game.\n", MAX_RANDOM_NUMBER);

    gameLoop(randomNumber);

    return 0;
}

void gameLoop(int randomNumber) {
    int guess, score=0;
    char input[INPUT_BUFFER_SIZE];

    while (1) {
        printf("Enter your guess: ");
        fgets(input, sizeof(input), stdin);

        if (sscanf(input, "%d", &guess) != 1) {
            printf("Invalid input. Please enter a valid number.\n");
            continue;
        }

        if (guess == 0) {
            printf("Game ended. Your final score: %d", score);
            break;
        }

        if (guess == randomNumber) {
            printf("Congratulations! You guessed the correct number!\n");
            score++;
            printf("Your current score: %d\n", score);
            continue;
        } else if (guess < randomNumber) {
            printf("Your guess is too low. Try again.\n");
        } else {
            printf("Your guess is too high. Try again.\n");
        }

        
    }
}
